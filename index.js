const RedCardsIssuedPerTeam = require("./src/server/red-cards-issued-per-team.cjs");
const matchesPlayedPerCity = require("./src/server/matches-played-per-city.cjs");
const top10Players = require("./src/server/top-10-players.cjs");
const matchesWonPerTeam = require("./src/server/matches-won-per-team.cjs");

const WorldCupPlayers = "./src/data/WorldCupPlayers.csv";
const WorldCupMatches = "./src/data/WorldCupMatches.csv";
const WorldCups = "./src/data/WorldCups.csv";

const csv = require("csvtojson");
const fs = require("fs");

// Q1 - Number of matches played per city

// csv()
//   .fromFile(WorldCupMatches)
//   .then((data) => {
//     const jsonString = JSON.stringify(matchesPlayedPerCity(data));
//     fs.writeFile(
//       "matches-played-per-city.json",
//       jsonString,
//       "utf8",
//       function (err) {
//         if (err) {
//           console.log("Error writing JSON file:", err);
//         } else {
//           console.log("JSON file created successfully!");
//         }
//       }
//     );
//   });

// Q2. Number of matches won per team

csv()
  .fromFile(WorldCups)
  .then((data) => {
    const jsonString = JSON.stringify(matchesWonPerTeam(data));
    fs.writeFile(
      "matches-won-per-team.json",
      jsonString,
      "utf8",
      function (err) {
        if (err) {
          console.log("Error writing JSON file:", err);
        } else {
          console.log("JSON file created successfully!");
        }
      }
    );
  });

// Q3 - Find the number of red cards issued per team in 2014 World Cup

// csv()
//   .fromFile(WorldCupMatches)
//   .then((match) => {
//     csv()
//       .fromFile(WorldCupPlayers)
//       .then((player) => {
//         const jsonString = JSON.stringify(
//           RedCardsIssuedPerTeam(match, player, "1998")
//         );
//         fs.writeFile(
//           "red-cards-issued-per-team.json",
//           jsonString,
//           "utf8",
//           function (err) {
//             if (err) {
//               console.log("Error writing JSON file:", err);
//             } else {
//               console.log("JSON file created successfully!");
//             }
//           }
//         );
//       });
//   });

// Q4 - Find the top 10 players with the highest probability of scoring a goal in a match

// csv()
//   .fromFile(WorldCupMatches)
//   .then((data) => {
//     const jsonString = JSON.stringify(top10Players(data));
//     fs.writeFile("top-10-players.json", jsonString, "utf8", function (err) {
//       if (err) {
//         console.log("Error writing JSON file:", err);
//       } else {
//         console.log("JSON file created successfully!");
//       }
//     });
//   });
