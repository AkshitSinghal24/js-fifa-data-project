function top10Players(playerData) {
  const goals = playerData.reduce((acc, player) => {
    if (player.Event.includes("G")) {
      let count = player.Event.split("G").length - 1;
      if (acc[player["Player Name"]]) {
        acc[player["Player Name"]] = acc[player["Player Name"]] + count;
      } else {
        acc[player["Player Name"]] = count;
      }
    }
    return acc;
  }, {});

  const matches = playerData.reduce((acc, player) => {
    if (acc[player["Player Name"]]) {
      acc[player["Player Name"]]++;
    } else {
      acc[player["Player Name"]] = 1;
    }
    return acc;
  }, {});

  const result = {};
  for (let key in goals) {
    let temp = goals[key] / matches[key];
    result[key] = temp;
  }

  const item = Object.entries(result);
  item.sort((a, b) => b[1] - a[1]);
  item.splice(10);
  const top10 = Object.fromEntries(item);

  return item;
}
module.exports = top10Players;
