function matchesPlayedPerCity(WorldCupsData) {

  const allCity = WorldCupsData.reduce((count, item) => {
    count[item.City] = (count[item.City] || 0) + 1;
    return count;
  }, {});

  return allCity;
}

module.exports = matchesPlayedPerCity;
