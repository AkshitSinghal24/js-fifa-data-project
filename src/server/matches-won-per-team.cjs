function matchesWonPerTeam(matchesData) {

  const matchesWon = matchesData.reduce((count, item) => {
    count[item.Winner] = (count[item.Winner] || 0) + 1;
    return count;
  }, {});

  return matchesWon;
}
module.exports = matchesWonPerTeam;
