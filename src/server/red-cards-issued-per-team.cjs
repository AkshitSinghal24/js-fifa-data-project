function RedCardsIssuedPerTeam(matchData, playerData, givenYear) {
  const allMatchId = new Set([
    ...matchData
      .filter((year) => year["Year"] == givenYear)
      .map((item) => item.MatchID),
  ]);

  const playerWithRed = playerData
    .filter((item) => allMatchId.has(item.MatchID))
    .filter((item) => item.Event.includes("R"));

  const teamsInitials = playerWithRed.reduce((team, item) => {
    team[item["Team Initials"]] = (team[item["Team Initials"]] || 0) + 1;
    return team;
  }, {});

  const allTeam = matchData.reduce((item, match) => {
    if (!item[match["Away Team Initials"]]) {
      item[match["Away Team Initials"]] = match["Away Team Name"];
    }
    if (!item[match["Home Team Initials"]]) {
      item[match["Home Team Initials"]] = match["Home Team Name"];
    }
    return item;
  }, {});

  const result = {};
  for (const key in teamsInitials) {
    result[allTeam[key]] = teamsInitials[key];
  }
  return result;
}

module.exports = RedCardsIssuedPerTeam;
